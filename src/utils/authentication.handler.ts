import { NextFunction, Request, Response } from "express";

export const authenticationHandler = (req: Request, res: Response, next: NextFunction) => {
  const userId: string | undefined = req.header('x-user-id');

	if (!userId) {
		const error = new Error('You must be authorized user');
		error.name = 'Unauthorized';
		throw error;
	}

	if (!checkUserExists(userId)) {
		const error = new Error('User is not authorized');
		error.name = 'Forbidden';
		throw error;
	}

  next();
};

const checkUserExists = (userId: string): boolean => {
  return userId === 'admin';
};