import { Request, Response } from 'express'
import * as ProductService from '../services/product.service'
import { ProductEntity, ResponseModel, } from '../models/models';

export const singleProductController = (req: Request, res: Response): void => {
	const productId: string = req.params.productId;
	const product: ProductEntity | undefined = ProductService.getSingleProduct(productId);
	if (product) {
		const response: ResponseModel = {
			data: product,
			error: null
		}
		res.status(200).json(response);
	} else {
		const error = new Error('No product with such id');
		error.name = 'Not Found'
		throw error;
	}
}

export const allProductsController = (req: Request, res: Response): void => {
	const products: ProductEntity[] = ProductService.getProductsList();
	const response: ResponseModel = {
		data: products,
		error: null
	}
	res.status(200).json(response);
}

