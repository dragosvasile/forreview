import { Request, Response } from "express";
import { OrderEntity, ResponseModel } from "../models/models";
import * as OrderService from '../services/order.service'

export const createOrderController = (req: Request, res: Response): void=> {
  const userId: string | undefined = req.header('x-user-id');
  if(!!userId) {
    const order: OrderEntity = OrderService.createAnOrder(userId);
    const response: ResponseModel = {
      data: order,
      error: null
    }
    res.status(200).json(response);
  }
}