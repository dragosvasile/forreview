import { Request, Response } from "express";
import { CartEntity, CartItemEntity, CartUpdateItemEntity, ResponseModel } from "../models/models";
import * as CartService from '../services/cart.service'
import Joi from 'joi';

const schema = Joi.object({
    productId: Joi.string().required(),
    count: Joi.number().integer().min(1).required()
});

export const getCartController = (req: Request, res: Response): void => {
  const userId: string = req.header('x-user-id') || '';
  const cart: CartEntity = CartService.getOrCreateUserCart(userId);
  const response: ResponseModel = {
    data: cart,
    error: null
  }
  res.status(200).json(response);
};

export const updateCartController = (req: Request, res: Response): void => {
  const userId: string = req.header('x-user-id') || '';
  const { error, value } = schema.validate(req.body);
  if (error) {
    const error = new Error('Validation Error');
    error.name = 'BadRequest';
    throw error;
  }
  const cart: CartUpdateItemEntity[] = [value as CartUpdateItemEntity];
  const updatedCart: CartEntity = CartService.updateCart(userId, cart);
  const response: ResponseModel = {
    data: updatedCart,
    error: null
  }
  res.status(200).json(response);
}

export const  deleteCartController = (req: Request, res: Response): void => {
  const userId: string = req.header('x-user-id') || '';
  const cartId: string = req.params.id;
  CartService.deleteCart(userId, cartId);
  const response: ResponseModel = {
    data: null,
    error: {
      message: 'success'
    }
  }
  res.status(200).json(response);
}
