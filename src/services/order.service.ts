import { CartEntity, OrderEntity } from "../models/models";
import { getCartByUserId } from "../repositories/cart.repository";
import { createOrder } from "../repositories/order.repository";
import { v4 as uuid } from 'uuid';

export const createAnOrder = (userId: string): OrderEntity => {
	const activeCart = getCartByUserId(userId);

	if (activeCart && activeCart.items.length < 1) {
		const error = new Error('Cart is empty');
		error.name = 'Bad request';
		throw error;
	} else if (activeCart) {
		const newOrder: OrderEntity = {
			id: uuid(),
			userId: userId,
			cartId: activeCart.id,
			items: activeCart.items,
			payment: {
				type: 'card',
				address: 'random',
				creditCard: '1111-1111-1111-1111',
			},
			delivery: {
				type: 'post',
				address: 'random',
			},
			comments: '',
			status: 'created',
			total: calculateOrderTotal(activeCart),
		}

		return createOrder(newOrder);
	} else {
		throw new Error('Internal error');
	}
};

const calculateOrderTotal = (cart: CartEntity): number => {
	return cart.items.reduce((total, item) => {
		const productTotal = item.product.price * item.count;
		return total + productTotal;
	}, 0);
}