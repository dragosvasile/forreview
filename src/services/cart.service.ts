import { CartEntity, CartItemEntity, CartUpdateItemEntity } from '../models/models';
import * as CartRepository from '../repositories/cart.repository';
import { getSingleProduct } from './product.service';

export const getOrCreateUserCart = (userId: string): CartEntity => {
	let existingCart: CartEntity | undefined = CartRepository.getCartByUserId(userId);

	if (!existingCart) {
		return CartRepository.addCart(userId);
	};

	return existingCart;
};

export const updateCart = (userId: string, cart: CartUpdateItemEntity[]): CartEntity => {
	const existingCart = CartRepository.getCartByUserId(userId);
	if (!existingCart) {
		const error = new Error('Cart was not found');
		error.name = 'NotFound';
		throw error;
	}

	for (const newItem of cart) {
		const existingItem = existingCart.items.find(item => item.product.id === newItem.productId);
		if (!existingItem) {
			const product = getSingleProduct(newItem.productId)
			if (product) {
				const cartItem: CartItemEntity = {
					product: product,
					count: newItem.count
				}
				existingCart.items.push(cartItem);
			}	
		} else {
			existingItem.count = newItem.count;
		}
	}

	const updatedCart = CartRepository.updateCart(existingCart);
	return updatedCart;
};

export const deleteCart = (userId: string, cartId: string) => {
	let existingCart: CartEntity | undefined = CartRepository.getCartByUserId(userId);
	if (!existingCart || existingCart.items.length === 0) {
		return;
	}
	updateCart(userId, []);

	CartRepository.deleteCart(cartId);
	// In order for the user to have one active cart at all times, we will create another one right after deleting the old one
	getOrCreateUserCart(userId);
};
