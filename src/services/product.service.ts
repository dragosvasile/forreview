import { ProductEntity } from "../models/models";
import { getProductById, getProducts } from "../repositories/product.repository";

export const getSingleProduct = (productId: string): ProductEntity | undefined => { 
    return getProductById(productId);
};

export const getProductsList = (): ProductEntity[] => {
    return getProducts();
};