import express from 'express';
import { singleProductController, allProductsController } from './controllers/product.controller';
import { authenticationHandler } from './utils/authentication.handler'
import { deleteCartController, getCartController, updateCartController } from './controllers/cart.controller';
import { createOrderController } from './controllers/order.controller';
import { errorHandler } from './utils/errorHandler';

const app = express();

app.use(express.json());
app.use(authenticationHandler);
app.get('/api/products/:productId', singleProductController);
app.get('/api/products', allProductsController);
app.get('/api/profile/cart', getCartController);
app.put('/api/profile/cart', updateCartController);
app.delete('/api/profile/cart', deleteCartController);
app.post('/api/profile/cart/checkout', createOrderController);
app.use(errorHandler);


app.listen({ port: 3000 }, () => {
    console.log('Server is started');
})