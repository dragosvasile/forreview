import { OrderEntity } from "../models/models";

const orders: OrderEntity[] = [];

export const createOrder = (order: OrderEntity):OrderEntity => {
    orders.push(order);
    return order;
};