import { CartEntity, CartItemEntity } from "../models/models";
import { v4 as uuidv4 } from 'uuid';

const carts: CartEntity[] = [];

export const addCart = (userId: string):CartEntity => {
    const cart = {
        id: uuidv4(),
        userId: userId,
        isDeleted: false,
        items: []
    };

    carts.push(cart);
    return cart;
}

export const updateCart = (cartItem: CartEntity) => {
    const existingCartIndex = carts.findIndex((cart) => cart.id === cartItem.id);
    if(existingCartIndex >= 0) {
        carts[existingCartIndex] = cartItem;
    }else {
        throw new Error("Cart not existing, please create")
    }

    return cartItem;
}

export const getCartByUserId = (userId: string): CartEntity | undefined => {
    for(const cart of carts) {
        if (cart.userId === userId && cart.isDeleted === false) {
            return cart;
        }
    }
}

export const deleteCart = (cartId: string) => {
    const cartIndex = carts.findIndex(cart => cart.id = cartId)
    carts[cartIndex].isDeleted = true;
}