import { ProductEntity } from "../models/models";

const products: ProductEntity[] = [
    {
        id: '1',
        title: 'book1',
        price: 100,
        description: 'expensive inexistent book'
    },
    {
        id: '2',
        title: 'book2',
        price: 99,
        description: 'expensive inexistent other book'
    }
];

export const getProductById = (productId: string): ProductEntity | undefined => {
    return products.find(prod => prod.id === productId);
}

export const getProducts = (): ProductEntity[] => {
    return products;
}