"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.errorHandler = void 0;
var ErrorStatus;
(function (ErrorStatus) {
    ErrorStatus[ErrorStatus["BadRequest"] = 400] = "BadRequest";
    ErrorStatus[ErrorStatus["NotFound"] = 404] = "NotFound";
    ErrorStatus[ErrorStatus["Unauthorized"] = 401] = "Unauthorized";
    ErrorStatus[ErrorStatus["Forbidden"] = 403] = "Forbidden";
    ErrorStatus[ErrorStatus["InternalServerError"] = 500] = "InternalServerError";
})(ErrorStatus || (ErrorStatus = {}));
const errorHandler = (err, req, res, next) => {
    if (res.headersSent) {
        return next(err);
    }
    let statusCode;
    const errorName = err.name;
    const errorMessage = err.message;
    if (errorName && ErrorStatus[errorName]) {
        statusCode = ErrorStatus[errorName];
    }
    else {
        statusCode = 500;
    }
    const responseModel = {
        data: null,
        error: { message: errorMessage,
            cause: err.cause,
            other: err.stack }
    };
    return res.status(statusCode).json(responseModel);
};
exports.errorHandler = errorHandler;
