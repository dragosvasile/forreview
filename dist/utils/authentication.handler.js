"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.authenticationHandler = void 0;
const authenticationHandler = (req, res, next) => {
    const userId = req.header('x-user-id');
    if (!userId) {
        const error = new Error('You must be authorized user');
        error.name = 'Unauthorized';
        throw error;
    }
    if (!checkUserExists(userId)) {
        const error = new Error('User is not authorized');
        error.name = 'Forbidden';
        throw error;
    }
    next();
};
exports.authenticationHandler = authenticationHandler;
const checkUserExists = (userId) => {
    return userId === 'admin';
};
