"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createOrder = void 0;
const orders = [];
const createOrder = (order) => {
    orders.push(order);
    return order;
};
exports.createOrder = createOrder;
