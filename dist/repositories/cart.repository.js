"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteCart = exports.getCartByUserId = exports.updateCart = exports.addCart = void 0;
const uuid_1 = require("uuid");
const carts = [];
const addCart = (userId) => {
    const cart = {
        id: (0, uuid_1.v4)(),
        userId: userId,
        isDeleted: false,
        items: []
    };
    carts.push(cart);
    console.log(carts);
    return cart;
};
exports.addCart = addCart;
const updateCart = (cartItem) => {
    const existingCartIndex = carts.findIndex((cart) => cart.id === cartItem.id);
    console.log('line 21 existingcartIndex:', existingCartIndex);
    if (existingCartIndex >= 0) {
        carts[existingCartIndex] = cartItem;
    }
    else {
        throw new Error("Cart not existing, please create");
    }
    return cartItem;
};
exports.updateCart = updateCart;
const getCartByUserId = (userId) => {
    for (const cart of carts) {
        console.log('line 32 entered iteration in repository');
        if (cart.userId === userId && cart.isDeleted === false) {
            return cart;
        }
    }
};
exports.getCartByUserId = getCartByUserId;
const deleteCart = (cartId) => {
    const cartIndex = carts.findIndex(cart => cart.id = cartId);
    carts[cartIndex].isDeleted = true;
};
exports.deleteCart = deleteCart;
