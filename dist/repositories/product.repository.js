"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getProducts = exports.getProductById = void 0;
const products = [
    {
        id: '1',
        title: 'book1',
        price: 100,
        description: 'expensive inexistent book'
    },
    {
        id: '2',
        title: 'book2',
        price: 99,
        description: 'expensive inexistent other book'
    }
];
const getProductById = (productId) => {
    return products.find(prod => prod.id === productId);
};
exports.getProductById = getProductById;
const getProducts = () => {
    return products;
};
exports.getProducts = getProducts;
