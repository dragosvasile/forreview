"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.allProductsController = exports.singleProductController = void 0;
const ProductService = __importStar(require("../services/product.service"));
const singleProductController = (req, res) => {
    const productId = req.params.productId;
    const product = ProductService.getSingleProduct(productId);
    if (product) {
        const response = {
            data: product,
            error: null
        };
        res.status(200).json(response);
    }
    else {
        const error = new Error('No product with such id');
        error.name = 'Not Found';
        throw error;
    }
};
exports.singleProductController = singleProductController;
const allProductsController = (req, res) => {
    const products = ProductService.getProductsList();
    const response = {
        data: products,
        error: null
    };
    res.status(200).json(response);
};
exports.allProductsController = allProductsController;
