"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteCartController = exports.updateCartController = exports.getCartController = void 0;
const CartService = __importStar(require("../services/cart.service"));
const joi_1 = __importDefault(require("joi"));
const schema = joi_1.default.object({
    productId: joi_1.default.string().required(),
    count: joi_1.default.number().integer().min(1).required()
});
const getCartController = (req, res) => {
    const userId = req.header('x-user-id') || '';
    const cart = CartService.getOrCreateUserCart(userId);
    const response = {
        data: cart,
        error: null
    };
    res.status(200).json(response);
};
exports.getCartController = getCartController;
const updateCartController = (req, res) => {
    const userId = req.header('x-user-id') || '';
    const { error, value } = schema.validate(req.body);
    if (error) {
        const error = new Error('Validation Error');
        error.name = 'BadRequest';
        throw error;
    }
    const cart = [value];
    const updatedCart = CartService.updateCart(userId, cart);
    const response = {
        data: updatedCart,
        error: null
    };
    res.status(200).json(response);
};
exports.updateCartController = updateCartController;
const deleteCartController = (req, res) => {
    const userId = req.header('x-user-id') || '';
    const cartId = req.params.id;
    CartService.deleteCart(userId, cartId);
    const response = {
        data: null,
        error: {
            message: 'success'
        }
    };
    res.status(200).json(response);
};
exports.deleteCartController = deleteCartController;
