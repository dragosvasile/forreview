"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteCart = exports.updateCart = exports.getOrCreateUserCart = void 0;
const CartRepository = __importStar(require("../repositories/cart.repository"));
const product_service_1 = require("./product.service");
const getOrCreateUserCart = (userId) => {
    let existingCart = CartRepository.getCartByUserId(userId);
    if (!existingCart) {
        return CartRepository.addCart(userId);
    }
    ;
    return existingCart;
};
exports.getOrCreateUserCart = getOrCreateUserCart;
const updateCart = (userId, cart) => {
    const existingCart = CartRepository.getCartByUserId(userId);
    if (!existingCart) {
        console.log('line 17 cart not found in service');
        const error = new Error('Cart was not found');
        error.name = 'NotFound';
        throw error;
    }
    for (const newItem of cart) {
        console.log('line 24 cart was found in service');
        const existingItem = existingCart.items.find(item => item.product.id === newItem.productId);
        if (!existingItem) {
            const product = (0, product_service_1.getSingleProduct)(newItem.productId);
            if (product) {
                const cartItem = {
                    product: product,
                    count: newItem.count
                };
                existingCart.items.push(cartItem);
            }
        }
        else {
            existingItem.count = newItem.count;
        }
    }
    const updatedCart = CartRepository.updateCart(existingCart);
    return updatedCart;
};
exports.updateCart = updateCart;
const deleteCart = (userId, cartId) => {
    let existingCart = CartRepository.getCartByUserId(userId);
    if (!existingCart || existingCart.items.length === 0) {
        return;
    }
    (0, exports.updateCart)(userId, []);
    CartRepository.deleteCart(cartId);
};
exports.deleteCart = deleteCart;
