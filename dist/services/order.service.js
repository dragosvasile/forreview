"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createAnOrder = void 0;
const cart_repository_1 = require("../repositories/cart.repository");
const order_repository_1 = require("../repositories/order.repository");
const uuid_1 = require("uuid");
const createAnOrder = (userId) => {
    const activeCart = (0, cart_repository_1.getCartByUserId)(userId);
    if (activeCart && activeCart.items.length < 1) {
        const error = new Error('Cart is empty');
        error.name = 'Bad request';
        throw error;
    }
    else if (activeCart) {
        const newOrder = {
            id: (0, uuid_1.v4)(),
            userId: userId,
            cartId: activeCart.id,
            items: activeCart.items,
            payment: {
                type: 'card',
                address: 'random',
                creditCard: '1111-1111-1111-1111',
            },
            delivery: {
                type: 'post',
                address: 'random',
            },
            comments: '',
            status: 'created',
            total: calculateOrderTotal(activeCart),
        };
        return (0, order_repository_1.createOrder)(newOrder);
    }
    else {
        throw new Error('Internal error');
    }
};
exports.createAnOrder = createAnOrder;
const calculateOrderTotal = (cart) => {
    return cart.items.reduce((total, item) => {
        const productTotal = item.product.price * item.count;
        return total + productTotal;
    }, 0);
};
