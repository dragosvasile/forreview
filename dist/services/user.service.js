"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createUser = exports.getUserById = void 0;
;
const userList = [];
const getUserById = (userId) => {
    const existingUser = userList.find(user => user.id === userId);
    return existingUser;
};
exports.getUserById = getUserById;
const createUser = (userData) => {
    if (!userList.find(user => user.id === userData.id)) {
        userList.push(userData);
    }
};
exports.createUser = createUser;
