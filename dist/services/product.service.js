"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getProductsList = exports.getSingleProduct = void 0;
const product_repository_1 = require("../repositories/product.repository");
const getSingleProduct = (productId) => {
    return (0, product_repository_1.getProductById)(productId);
};
exports.getSingleProduct = getSingleProduct;
const getProductsList = () => {
    return (0, product_repository_1.getProducts)();
};
exports.getProductsList = getProductsList;
