"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const product_controller_1 = require("./controllers/product.controller");
const authentication_handler_1 = require("./utils/authentication.handler");
const cart_controller_1 = require("./controllers/cart.controller");
const order_controller_1 = require("./controllers/order.controller");
const errorHandler_1 = require("./utils/errorHandler");
const app = (0, express_1.default)();
app.use(express_1.default.json());
app.use(authentication_handler_1.authenticationHandler);
app.get('/api/products/:productId', product_controller_1.singleProductController);
app.get('/api/products', product_controller_1.allProductsController);
app.get('/api/profile/cart', cart_controller_1.getCartController);
app.put('/api/profile/cart', cart_controller_1.updateCartController);
app.delete('/api/profile/cart', cart_controller_1.deleteCartController);
app.post('/api/profile/cart/checkout', order_controller_1.createOrderController);
app.use(errorHandler_1.errorHandler);
app.listen({ port: 3000 }, () => {
    console.log('Server is started');
});
