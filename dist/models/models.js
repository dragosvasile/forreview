"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const bookProduct = {
    id: '51422fcd-0366-4186-ad5b-c23059b6f64f',
    title: 'Book',
    description: 'A very interesting book',
    price: 100
};
const cartItem = {
    product: bookProduct,
    count: 2,
};
const cart = {
    id: '1434fec6-cd85-420d-95c0-eee2301a971d',
    userId: '0fe36d16-49bc-4aab-a227-f84df899a6cb',
    isDeleted: false,
    items: [cartItem],
};
const order = {
    id: 'dffd6fa8-be6b-47f6-acff-455612620ac2',
    userId: '0fe36d16-49bc-4aab-a227-f84df899a6cb',
    cartId: '',
    items: cart.items,
    payment: {
        type: 'paypal',
        address: undefined,
        creditCard: undefined
    },
    delivery: {
        type: 'post',
        address: undefined
    },
    comments: '',
    status: 'created',
    total: 2,
};
